﻿using UnityEngine;

public class DecorationGate : ObjectPoolEntity 
{

	[SerializeField] private Light _light;

	public Light Light { get { return _light; } set { _light = value; } }


}

