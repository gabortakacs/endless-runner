﻿using UnityEngine;

public class Platform : ObjectPoolEntity
{

	[SerializeField] private float _length;
	[SerializeField] private Transform[] _powerUpPositions;

	public float Length { get { return _length; } }

	public Vector3 GetRandomPowerUpPosition()
	{
		return _powerUpPositions[Random.Range(0, _powerUpPositions.Length)].position;
	}

}

