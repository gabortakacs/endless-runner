﻿using UnityEngine;

public class DecorationGatePool : ObjectPool
{

	public override ObjectPoolEntity Poll()
	{
		if (Count == 0)
		{
			Debug.Log("<color=red> ERROR! Object pool is empty!!! </color>");
			return null;
		}
		DecorationGate gate = (DecorationGate)_pool.Dequeue();
		return gate;
	}
}
