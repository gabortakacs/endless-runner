﻿using UnityEngine;
using Random = UnityEngine.Random;

public class MapBuilder : MonoBehaviour 
{

	[Header("Platforms")]
	[SerializeField] private Vector3 _startPosition = Vector3.zero;
	[SerializeField] private PlatformPool[] _platformPools;
	[SerializeField] private PlatformPool[] _platformWithObstaclePools;
	[SerializeField] private int _platformAmountInGame = 20;
	[SerializeField] private int _minPlatformAmountBetweenObstacle = 3;
	[SerializeField] [Range(0.0f, 1.0f)] private float _chanceToObstacle = 0.5f;

	[Header("Decoration Gates")]
	[SerializeField] private DecorationGatePool _decorationGatePool;
	[SerializeField] private int _gatesAmountInGame = 30;
	[SerializeField] private float _distanceBetweenGates = 20;
	[SerializeField] private Gradient _decorationGateColor;


	[Header("Power Ups")]
	[SerializeField] private PowerUpPool[] _powerUpPools;
	[SerializeField] [Range(0.0f, 1.0f)] private float _powerUpSpawnChangePerPlatform = 0.5f;

	private float _currentGatePosition = 0;
	private float _currentPlatformPosition = 0;
	private int _platformAfterObstacleCounter = 0;

	private int[] _powerUpChances;

	private void Awake()
	{
		CalculatePowerUpSpawnChances();
	}

	private void Start()
	{
		GenerateStartMap();
		MapDestroyer.OnPlatformDestroyed += OnPlatformDestroyed;
		MapDestroyer.OnDecorationGateDestroyed += OnDecorationGatesDestroyed;
	}

	private void CalculatePowerUpSpawnChances()
	{
		_powerUpChances = new int[_powerUpPools.Length];
		int _powerUpChanceSumma = 0;
		for (int i = 0; i < _powerUpChances.Length; ++i)
		{
			_powerUpChanceSumma += _powerUpPools[i].ChanceToSpawn;
			_powerUpChances[i] = _powerUpChanceSumma;
		}
	}

	private void GenerateStartMap()
	{
		PutStartPlatforms();
		PutStartDecorationGates();
	}

	private void PutStartDecorationGates()
	{
		for (int i = 0; i < _gatesAmountInGame; ++i)
		{
			PutDecorationGate();
		}
	}

	private void PutDecorationGate()
	{
		DecorationGate gate = (DecorationGate)_decorationGatePool.Poll();
		gate.transform.position = _startPosition + Vector3.forward * _currentGatePosition;
		_currentGatePosition += _distanceBetweenGates;
		float colorNo = Mathf.InverseLerp(Player.Instance.StartSpeed, Player.Instance.MaxSpeed, Player.Instance.Speed);
		gate.Light.color = _decorationGateColor.Evaluate(colorNo);
		gate.gameObject.SetActive(true);
	}

	private void PutStartPlatforms()
	{
		PutPlatform((Platform)_platformPools[0].Poll());
		for (int i = 0; i < _platformAmountInGame; ++i)
		{
			PutRandomPlatform();
		}
	}

	private void PutRandomPlatform()
	{
		Platform platform;
		if (_platformAfterObstacleCounter < _minPlatformAmountBetweenObstacle || _platformWithObstaclePools.Length == 0 || Random.Range(0.0f, 1.0f) > _chanceToObstacle)
		{
			int poolIndex = Random.Range(0, _platformPools.Length);
			platform = (Platform)_platformPools[poolIndex].Poll();
			_platformAfterObstacleCounter++;
		}
		else
		{
			int poolIndex = Random.Range(0, _platformWithObstaclePools.Length);
			platform = (Platform)_platformWithObstaclePools[poolIndex].Poll();
			_platformAfterObstacleCounter = 0;
		}
		PutPlatform(platform);
	}

	private void PutPlatform(Platform platform)
	{
		platform.transform.position = _startPosition + Vector3.forward * _currentPlatformPosition;
		_currentPlatformPosition += platform.Length;
		platform.gameObject.SetActive(true);
		SpawnRandomPowerUp(platform);
	}

	private void SpawnRandomPowerUp(Platform platform)
	{
		if (Random.Range(0.0f, 1.0f) > _powerUpSpawnChangePerPlatform)
			return;
		int rnd = Random.Range(0, _powerUpChances[_powerUpChances.Length - 1]);
		for (int i = 0; i < _powerUpChances.Length; ++i)
		{
			if (_powerUpChances[i] > rnd)
			{
				if(SpawnPowerUp(platform, i))
					break;
			}
		}
	}

	private bool SpawnPowerUp(Platform platform, int index)
	{
		Vector3 pos = platform.GetRandomPowerUpPosition();
		PowerUp powerUp = (PowerUp)_powerUpPools[index].Poll();
		if (powerUp == null)
			return false;
		powerUp.transform.position = pos;
		powerUp.gameObject.SetActive(true);
		return true;
	}

	private void OnPlatformDestroyed()
	{
		PutRandomPlatform();
	}

	private void OnDecorationGatesDestroyed()
	{
		PutDecorationGate();
	}

	public void OnDestroy()
	{
		//if (!MapDestroyer.IsNull)
		//{
			MapDestroyer.OnPlatformDestroyed -= OnPlatformDestroyed;
			MapDestroyer.OnDecorationGateDestroyed -= OnDecorationGatesDestroyed;
		//}
		//base.OnDestroy();
	}
}

