﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformWithObstacle : Platform 
{

	[SerializeField] private Transform _obstaclePosition;
	[SerializeField] private GameObject[] _fullBlockObstacles;
	[SerializeField] private GameObject[] _notFullBlockObstacles;
	[SerializeField] private float _laneDistance;

	private void Awake()
	{
		int fullBlockedLane = Random.Range(0, 3);
		int notFullBlockedLane = Random.Range(0, 4 - fullBlockedLane);
		List<GameObject> obstacles = new List<GameObject>();
		for (int i = 0; i < fullBlockedLane; ++i)
		{
			obstacles.Add(_fullBlockObstacles[Random.Range(0, _fullBlockObstacles.Length)]);
		}
		for (int i = 0; i < notFullBlockedLane; ++i)
		{
			obstacles.Add(_notFullBlockObstacles[Random.Range(0, _notFullBlockObstacles.Length)]);
		}
		int lane = -1;
		while(obstacles.Count > 0)
		{
			int index = Random.Range(0, obstacles.Count);
			GameObject tmp = Instantiate(obstacles[index]);
			tmp.transform.parent = transform;
			Vector3 position = _obstaclePosition.localPosition;
			position.x = _laneDistance * lane;
			tmp.transform.localPosition = position;
			obstacles.RemoveAt(index);
			++lane;
		}
	}




}

