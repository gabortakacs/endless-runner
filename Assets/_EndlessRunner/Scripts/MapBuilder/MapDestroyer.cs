﻿using UnityEngine;

public delegate void PlatformDestroyed();
public delegate void DecorationGateDestroyed();

public class MapDestroyer : MonoBehaviour
{

	public static PlatformDestroyed OnPlatformDestroyed;
	public static DecorationGateDestroyed OnDecorationGateDestroyed;

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMasks.PlatfromSingleLayerMask)
		{
			Platform platform = other.GetComponent<Platform>();
			if (platform.Owner)
			{
				platform.Owner.Push(platform);
			}
			else
			{
				Debug.Log("<color=red>ERROR! PLATFORM DOESN'T HAVE OWNER! DESTROYED</color>");
				Destroy(platform.gameObject);
			}
			if (OnPlatformDestroyed != null)
				OnPlatformDestroyed.Invoke();
		}
		else if (other.gameObject.layer == LayerMasks.GateSingleLayerMask)
		{
			ObjectPoolEntity gate = other.GetComponent<ObjectPoolEntity>();
			if (gate.Owner)
			{
				gate.Owner.Push(gate);
			}
			else
			{
				Debug.Log("<color=red>ERROR! GATE DOESN'T HAVE OWNER! DESTROYED</color>");
				Destroy(gate.gameObject);
			}
			if (OnDecorationGateDestroyed != null)
				OnDecorationGateDestroyed.Invoke();
		}
		else if (other.gameObject.layer == LayerMasks.PowerUpSingleLayerMask)
		{
			PowerUp powerUp = other.GetComponent<PowerUp>();
			if (powerUp.Owner)
			{
				powerUp.Owner.Push(powerUp);
			}
			else
			{
				Debug.Log("<color=red>ERROR! POWERUP DOESN'T HAVE OWNER! DESTROYED</color>");
				Destroy(powerUp.gameObject);
			}
		}
	}



}

