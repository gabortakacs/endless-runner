﻿using UnityEngine;

public class PlatformPool : ObjectPool 
{
	[SerializeField] private GameObject _deactivatorZoneTarget;

	protected override void Awake()
	{
		for (int i = 0; i < _amount; ++i)
		{
			Platform tmp = (Platform) Instantiate(_entityPrefab);
			tmp.gameObject.transform.parent = transform;
			tmp.gameObject.SetActive(false);
			tmp.Owner = this;
			DeactivatorZone[] dZones = tmp.GetComponentsInChildren<DeactivatorZone>();
			for (int j = 0; j < dZones.Length; ++j)
			{
				dZones[j].Target = _deactivatorZoneTarget;
			}
			_pool.Enqueue(tmp);
		}
	}

}

