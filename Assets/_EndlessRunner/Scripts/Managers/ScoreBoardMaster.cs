﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardMaster : Singleton<ScoreBoardMaster>
{

	[SerializeField] private int _scoreBoardLength = 10;
	[SerializeField] private Transform _scoreBoardRowsParent;
	[SerializeField] private Text _scoreBoardRowPrefab;

	private List<int> _scores = new List<int>();
	private List<Text> _scoreBoardRows = new List<Text>();

	private void Awake()
	{
		ReadScoresFromPlayerPrefs();
		if (_scoreBoardRowsParent == null || _scoreBoardRowPrefab == null)
			return;
		InitScoreBoardRows();
		RefreshScoreBoard();
	}

	public void AddScore(int score)
	{
		if (_scores[_scoreBoardLength - 1] < score)
		{
			_scores[_scoreBoardLength - 1] = score;
			SaveScoreToPlayerPrefs();
			RefreshScoreBoard();
		}
	}

	public void ResetScoreBoard()
	{
		for (int i = 0; i < _scoreBoardLength; ++i)
		{
			_scores[i] = 0;
			PlayerPrefs.SetInt("scoreBoard" + i, 0);
		}
		RefreshScoreBoard();
	}

	private void InitScoreBoardRows()
	{
		for(int i = 0; i < _scoreBoardLength; ++i)
		{
			_scoreBoardRows.Add(Instantiate(_scoreBoardRowPrefab, _scoreBoardRowsParent));
		}
	}

	private void RefreshScoreBoard()
	{
		for(int i = 0; i <_scoreBoardRows.Count; ++i)
		{
			_scoreBoardRows[i].text = (i + 1).ToString() + ". " + _scores[i].ToString();
		}
	}

	private void ReadScoresFromPlayerPrefs()
	{
		for (int i = 0; i < _scoreBoardLength; ++i)
		{
			_scores.Add(PlayerPrefs.GetInt("scoreBoard" + i, 0));
		}
		SortScores();
	}

	private void SaveScoreToPlayerPrefs()
	{
		SortScores();
		for (int i = 0; i < _scoreBoardLength; ++i)
		{
			PlayerPrefs.SetInt("scoreBoard" + i, _scores[i]);
		}
	}

	private void SortScores()
	{
		_scores.Sort((p1, p2) => (-p1).CompareTo(-p2));

	}
}

