﻿using UnityEngine;

public delegate void SwipeDetected(SwipeDirection direction);

public class SwipeManager : Singleton<SwipeManager>
{
	[SerializeField] private float _minSwipeDistance = 100.0f;

	private Vector2 _touchStartPoint;
	private bool _invoked = false;

	public event SwipeDetected OnSwipeDetected;

	private void Start()
	{
		Input.multiTouchEnabled = false;
	}
	
	private void Update()
	{
		if (Input.touchCount == 0)
			return;

		GetSwipe();
	}

	private void GetSwipe()
	{
		if (Input.touches[0].phase == TouchPhase.Began)
		{
			_touchStartPoint = Input.touches[0].position;
			_invoked = false;
		}
		else if (Input.touches[0].phase == TouchPhase.Moved )
		{
			SwipeDirection dir = ReadSwipeDirection();
			if (dir != SwipeDirection.None && OnSwipeDetected != null && !_invoked)
			{
				OnSwipeDetected.Invoke(dir);
				_invoked = true;
			}
		}
		else if(Input.touches[0].phase == TouchPhase.Ended)
		{
			_invoked = false;
		}
	}

	private SwipeDirection ReadSwipeDirection()
	{
		Vector2 finalPoint = Input.touches[0].position;
		Vector3 direction = finalPoint - _touchStartPoint;
		if (direction.magnitude >= _minSwipeDistance)
		{
			if (Mathf.Abs(_touchStartPoint.x - finalPoint.x) > Mathf.Abs(_touchStartPoint.y - finalPoint.y))
			{
				if (_touchStartPoint.x <= finalPoint.x)
				{
					return SwipeDirection.Right;
				}
				else
				{
					return SwipeDirection.Left;
				}
			}
			else
			{
				if (_touchStartPoint.y <= finalPoint.y)
				{
					return SwipeDirection.Up;
				}
				else
				{
					return SwipeDirection.Down;
				}
			}
		}
		return SwipeDirection.None;
	}
}

public enum SwipeDirection
{
	None,
	Up,
	Down,
	Left,
	Right
}
