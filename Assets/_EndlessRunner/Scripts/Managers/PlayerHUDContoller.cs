﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDContoller : MonoBehaviour 
{

	[SerializeField] private Text _speedText;
	[SerializeField] private Text _score;

	private void Start()
	{
		GameMaster.OnScoreUpdated += OnScoreUpdated;
	}

	private void Update()
	{
		_speedText.text = ((int)Player.Instance.Rigidbody.velocity.z).ToString();
	}

	private void OnScoreUpdated(float newScore)
	{
		if(_score)
			_score.text = ((int)newScore).ToString();
	}

	private void OnDestroy()
	{
		if (!GameMaster.IsNull)
			GameMaster.OnScoreUpdated -= OnScoreUpdated;
	}

}

