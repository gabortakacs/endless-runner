﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AchievementMaster : Singleton<AchievementMaster>
{

	[SerializeField] private GameObject _elementsParent;

	private Dictionary<AchievementType, List<AchievementElement>> _elementDictionary = new Dictionary<AchievementType, List<AchievementElement>>();
	private int _highestScore;
	private int _highestTimeScore;
	private int _pickUpAll;
	private int _pickupNoGravity;
	private int _pickUpScore;
	private int _pickUpIndestructible;

	public int HighestTimeScore { get { return _highestTimeScore; } }

	private void Awake()
	{
		AddElementsToDictionary();
	}

	private void Start()
	{
		ReadAllHighestScoreFromPlayerprefs();
		RefreshAchievementElemetsPerformed();
		GameMaster.OnScoreUpdated += UpdateHighestScore;
		GameMaster.OnTimeScoreUpdated += UpdateHighestTimeScore;
		Player.OnPowerUpPickedUp += UpdatePickups;
	}

	public void SaveAllHighestScoreFromPlayerprefs()
	{
		PlayerPrefs.SetInt("highestScore", _highestScore);
		PlayerPrefs.SetInt("highestTimeScore", _highestTimeScore);
		PlayerPrefs.SetInt("pickUpAll", _pickUpAll);
		PlayerPrefs.SetInt("pickupNoGravity", _pickupNoGravity);
		PlayerPrefs.SetInt("pickUpScore", _pickUpScore);
		PlayerPrefs.SetInt("pickUpIndestructible", _pickUpIndestructible);
	}

	public void RefreshAchievementElemetsPerformed()
	{
		foreach (AchievementType type in _elementDictionary.Keys)
		{
			foreach (AchievementElement element in _elementDictionary[type])
			{
				switch (type)
				{
					case AchievementType.Score:
						element.Performed = (_highestScore >= element.ConditionScore);
						break;
					case AchievementType.Time:
						element.Performed = (_highestTimeScore >= element.ConditionScore);
						break;
					case AchievementType.PowerUpAll:
						element.Performed = (_pickUpAll >= element.ConditionScore);
						break;
					case AchievementType.PowerUpNoGravity:
						element.Performed = (_pickupNoGravity >= element.ConditionScore);
						break;
					case AchievementType.PowerUpScore:
						element.Performed = (_pickUpScore >= element.ConditionScore);
						break;
					case AchievementType.PowerUpIndestructiblePowerUp:
						element.Performed = (_pickUpIndestructible >= element.ConditionScore);
						break;
				}
			}
		}
	}

	public void ResetAllAchievements()
	{
		_highestScore = 0;
		_highestTimeScore = 0;
		_pickUpAll = 0;
		_pickupNoGravity = 0;
		_pickUpScore = 0;
		_pickUpIndestructible = 0;
		SaveAllHighestScoreFromPlayerprefs();
		RefreshAchievementElemetsPerformed();
	}

	private void ReadAllHighestScoreFromPlayerprefs()
	{
		_highestScore = PlayerPrefs.GetInt("highestScore", 0);
		_highestTimeScore = PlayerPrefs.GetInt("_highestTimeScore", 0);
		_pickUpAll = PlayerPrefs.GetInt("pickUpAll", 0);
		_pickupNoGravity = PlayerPrefs.GetInt("pickupNoGravity", 0);
		_pickUpScore = PlayerPrefs.GetInt("pickUpScore", 0);
		_pickUpIndestructible = PlayerPrefs.GetInt("pickUpIndestructible", 0);
	}

	private void UpdateHighestScore(float newScore)
	{
		if ( (int)newScore > _highestScore)
			_highestScore = (int)newScore;
		//print(_highestScore);
	}

	private void UpdateHighestTimeScore(int newScore)
	{
		if (newScore > _highestTimeScore)
			_highestTimeScore = newScore;
	}

	private void UpdatePickups(PowerUpType type)
	{
		switch (type)
		{
			case PowerUpType.None:
				return;
			case PowerUpType.NoGravity:
				++_pickupNoGravity;
				break;
			case PowerUpType.Score:
				++_pickUpScore;
				break;
			case PowerUpType.Indestructible:
				++_pickUpIndestructible;
				break;
		}
		++_pickUpAll;
	}

	private void AddElementsToDictionary()
	{
		AchievementElement[] elements = _elementsParent.GetComponentsInChildren<AchievementElement>();
		foreach (AchievementElement element in elements)
		{
			AddToDictionary(element);
		}
	}

	private void AddToDictionary(AchievementElement element)
	{
		List<AchievementElement> elements;

		if(_elementDictionary.TryGetValue(element.ConditionType, out elements))
		{
			_elementDictionary.Remove(element.ConditionType);
		}
		else
		{
			elements = new List<AchievementElement>();
		}
		elements.Add(element);
		_elementDictionary.Add(element.ConditionType, elements);
	}

	public override void OnDestroy()
	{
		if (!GameMaster.IsNull)
		{
			GameMaster.OnScoreUpdated -= UpdateHighestScore;
			GameMaster.OnTimeScoreUpdated -= UpdateHighestTimeScore;
		}
		if(!Player.IsNull)
			Player.OnPowerUpPickedUp += UpdatePickups;
		base.OnDestroy();
	}
}