﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public delegate void ScoreUpdated(float newScore);
public delegate void TimeScoreUpdated(int newScore);

public class GameMaster : Singleton<GameMaster> {

	[SerializeField] private float _normalTimeScale = 1.0f;
	[SerializeField] private float _pausedTimeScale = 0.000001f;
	[SerializeField] private int _mainMenuBuildIndex = 0;
	[SerializeField] private int _endlessSceneBuildIndex = 1;
	[SerializeField] private float _scoreMultiplier = 0.1f;
	[SerializeField] private float _speedMultiplier = 0.0001f;

	private bool _isPaused = false;
	private bool _isGameOver = false;
	private float _score = 0;
	private float _timeScore = 0;

	public bool IsPaused { get { return _isPaused; } }
	public bool IsGameOver { get { return _isGameOver; } }
	public float Score { get { return _score; } }
	public float TimeScore { get { return _timeScore; } }

	public static ScoreUpdated OnScoreUpdated;
	public static TimeScoreUpdated OnTimeScoreUpdated;

	private void Awake()
	{
		Time.timeScale = _normalTimeScale;
	}

	private void Start()
	{
		InvokeRepeating("InvokeTimeScore", AchievementMaster.Instance.HighestTimeScore, 1.0f);
	}

	private void Update()
	{
		UpdateScore();
		UpdateTimeScore();
		UpdateSpeed();
	}

	public void AddScore(float point)
	{
		_score += point;
		if (OnScoreUpdated != null)
			OnScoreUpdated.Invoke(_score);
	}

	public void GameOver()
	{
		MenuMaster.Instance.GameOverMenu.SetActive(true);
		_isGameOver = true;
	}

	public void OnPlayerDied()
	{
		GameOver();
	}

	public void StartGame()
	{
		LoadScene(_endlessSceneBuildIndex);
	}

	public void BackToMainMenu()
	{
		LoadScene(_mainMenuBuildIndex);
	}
	public void LoadScene(int index)
	{
		SceneManager.LoadScene(index);
	}

	public void RestartScene()
	{
		AchievementMaster.Instance.SaveAllHighestScoreFromPlayerprefs();
		AchievementMaster.Instance.RefreshAchievementElemetsPerformed();
		LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void QuitGame()
	{
		AchievementMaster.Instance.SaveAllHighestScoreFromPlayerprefs();
		AchievementMaster.Instance.RefreshAchievementElemetsPerformed();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void Pause()
	{
		AchievementMaster.Instance.SaveAllHighestScoreFromPlayerprefs();
		AchievementMaster.Instance.RefreshAchievementElemetsPerformed();
		_isPaused = true;
		Time.timeScale = _pausedTimeScale;
	}

	public void Resume()
	{
		Time.timeScale = _normalTimeScale;
		_isPaused = false;
	}

	public void Continue()
	{
		throw new NotImplementedException();
	}

	public void PlayerDie()
	{
		AchievementMaster.Instance.SaveAllHighestScoreFromPlayerprefs();
		AchievementMaster.Instance.RefreshAchievementElemetsPerformed();
		ScoreBoardMaster.Instance.AddScore((int)_score);
		GameOver();
	}

	private void UpdateScore()
	{
		if (Player.IsNull)
			return;
		AddScore(Player.Instance.Speed * Time.deltaTime * _scoreMultiplier);
	}

	private void UpdateTimeScore()
	{
		if (Player.IsNull || _isPaused || _isGameOver)
			return;
		_timeScore += Time.deltaTime;
	}


	private void UpdateSpeed()
	{
		if (Player.IsNull)
			return;
		Player.Instance.Speed = Mathf.Lerp(Player.Instance.StartSpeed, Player.Instance.MaxSpeed, _score * _speedMultiplier);
	}

	void InvokeTimeScore()
	{
		if(OnTimeScoreUpdated != null)
			OnTimeScoreUpdated.Invoke((int)_timeScore);
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}
}
