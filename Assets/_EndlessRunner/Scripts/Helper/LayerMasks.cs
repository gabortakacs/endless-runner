﻿using UnityEngine;

public static class LayerMasks 
{
	//public static int PlayerLayerMask = LayerMask.GetMask("Player") + 1;

	public static int PlayerSingleLayerMask = LayerMask.NameToLayer("Player");
	public static int PlatfromSingleLayerMask = LayerMask.NameToLayer("Platform");
	public static int GateSingleLayerMask = LayerMask.NameToLayer("Gate");
	public static int PowerUpSingleLayerMask = LayerMask.NameToLayer("PowerUp");
}
