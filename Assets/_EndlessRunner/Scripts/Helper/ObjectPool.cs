﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{ 
	[SerializeField] protected ObjectPoolEntity _entityPrefab;
	[SerializeField] protected int _amount;

	protected Queue<ObjectPoolEntity> _pool = new Queue<ObjectPoolEntity>();

	public int Count { get { return _pool.Count; } }

	protected virtual void Awake()
	{
		for(int i = 0; i < _amount; ++i)
		{
			ObjectPoolEntity tmp = Instantiate(_entityPrefab);
			tmp.gameObject.transform.parent = transform;
			tmp.gameObject.SetActive(false);
			tmp.Owner = this;
			_pool.Enqueue(tmp);
		}
	}

	public void Push(ObjectPoolEntity obj)
	{
		obj.gameObject.SetActive(false);
		_pool.Enqueue(obj);
	}

	public virtual ObjectPoolEntity Poll()
	{
		if (Count == 0)
		{
			Debug.Log("<color=red> ERROR! Object pool is empty!!! </color>");
			return null;
		}
		return _pool.Dequeue();
	}

}

