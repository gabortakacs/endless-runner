﻿using System.Collections.Generic;
using UnityEngine;

public class TesterScript : MonoBehaviour 
{
	bool cheat = false;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.F))
		{
			Player.Instance.Speed += 10;
		}
		if (Input.GetKeyDown(KeyCode.G))
		{
			Player.Instance.UseNoGravityPowerUp(15f);
		}
		if (Input.GetKeyDown(KeyCode.H))
		{
			Player.Instance.UseIndestructiblePowerUp(15f);
		}
		if (Input.GetKeyDown(KeyCode.P))
		{
			cheat = !cheat;
			Debug.Log("cheat: " + (cheat ? "ON" : "OFF"));
		}
		if(cheat)
		{
			Player.Instance.UseNoGravityPowerUp(15f);
			Player.Instance.UseIndestructiblePowerUp(15f);
		}
	}

}

