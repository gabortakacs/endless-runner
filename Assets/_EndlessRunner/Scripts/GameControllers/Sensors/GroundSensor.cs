﻿using UnityEngine;

public class GroundSensor : MonoBehaviour 
{

	private Player _player;

	public Player Player { set { _player = value; } }
	 
	private void OnTriggerStay(Collider other)
	{
		_player.IsGrounded = true;
	}

	private void OnTriggerExit(Collider other)
	{
		_player.IsGrounded = false;
	}


}

