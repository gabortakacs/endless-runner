﻿using UnityEngine;

public class LaneSensor : MonoBehaviour 
{

	private int _objectNumberInTrigger = 0;

	public bool IsFree
	{
		get
		{
			if (_objectNumberInTrigger < 0)
				Debug.Log("<color=red>LANESENSOR BUG</color>");
			return _objectNumberInTrigger == 0;
		}
	}


	private void OnTriggerEnter(Collider other)
	{
		_objectNumberInTrigger++;
	}

	private void OnTriggerExit(Collider other)
	{
		_objectNumberInTrigger--;
	}

}

