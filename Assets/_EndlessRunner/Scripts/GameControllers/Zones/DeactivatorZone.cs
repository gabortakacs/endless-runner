﻿using UnityEngine;

public class DeactivatorZone : MonoBehaviour 
{

	[SerializeField] private GameObject _target;

	public GameObject Target { set { _target = value; } }

	private void OnTriggerEnter(Collider other)
	{
		if(!Player.Instance.NoGravityPowerUpOn)
		_target.SetActive(false);
	}



}

