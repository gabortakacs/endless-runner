﻿using UnityEngine;

public class KillZone : MonoBehaviour 
{
	[SerializeField] private bool _baseKillingZone = false;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMasks.PlayerSingleLayerMask && (_baseKillingZone || !_baseKillingZone && !Player.Instance.Indestructible))
		{
			GameMaster.Instance.PlayerDie();
		}
	}



}

