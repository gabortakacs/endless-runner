﻿using System.Collections;
using UnityEngine;

public delegate void PowerUpPickedUp(PowerUpType type);

public class Player : Singleton<Player> 
{
	[SerializeField] private GameObject _modelHolder;
	[SerializeField] private Animator _animator;
	[SerializeField] private Rigidbody _rigidbody;


	[Header("Speed and lane changing")]
	[SerializeField] private float _speed = 30.0f;
	[SerializeField] private float _maxSpeed = 100.0f;
	[SerializeField] private float _animatorSpeedMultiplier = 0.1f;
	[SerializeField] private float _laneChangeSpeedRate = 10f;

	[Header("Sensors")]
	[SerializeField] private GroundSensor _groundSensor;
	[SerializeField] private LaneSensor _rightSensor;
	[SerializeField] private LaneSensor _leftSensor;

	[Header("Jump")]
	[SerializeField] private float _jumpForce = 10f;
	[SerializeField] private float _groundSensorDelayAfterJump = 0.5f;

	[Header("Slice")]
	[SerializeField] private float _sliceDuration = 3.0f;
	[SerializeField] private float _sliceScale = 0.5f;

	[Header("PowerUps")]
	[SerializeField] private GameObject _noGravitySign;
	[SerializeField] private GameObject _indestructibleSign;
	[SerializeField] [Range(0, 1)] private float _powerBlinkingPercent = 0.8f;

	private bool _isGrounded = false;
	private bool _isSliding = false;
	private bool _jump = false;
	private bool _laneChangeInProgress = false;
	private bool _noGravityPowerUpOn = false;
	private bool _indestructible = false;
	private float _middleLaneX = 0.0f;
	private float _laneChangeDuration;
	private float _laneDistance = 5f;
	private float _startSpeed;
	private int _currentLane = 0;
	private Vector3 _lastPosition;
	public bool _touchScreen = false;

	private Coroutine _noGravityRoutine = null;
	private Coroutine _indestructibleRoutine = null;

	public static PowerUpPickedUp OnPowerUpPickedUp;

	public bool NoGravityPowerUpOn { get { return _noGravityPowerUpOn; } }
	public bool Indestructible { get { return _indestructible; } }
	public float StartSpeed { get { return _startSpeed; } }
	public float MaxSpeed { get { return _maxSpeed; } }
	public float MiddleLaneX { set { _middleLaneX = value; } }
	public float LaneDistance { set { _laneDistance = value; } }
	public Rigidbody Rigidbody { get { return _rigidbody; } }

	public float Speed {
		get
		{
			return _speed;
		}
		set
		{
			_speed = value;
			_animator.speed = _speed * _animatorSpeedMultiplier;
			_laneChangeDuration = _laneChangeSpeedRate / _speed;
		}
	}

	public bool IsGrounded
	{
		set
		{
			_isGrounded = value;
		}
	}

	private void Awake()
	{
		_groundSensor.Player = this;
		Vector3 pos = transform.position;
		pos.x = _middleLaneX + _laneDistance;
		_rightSensor.transform.position = pos;
		pos.x = _middleLaneX -  _laneDistance;
		_leftSensor.transform.position = pos;
		Vector3 _lastPosition = transform.position;
		Speed = _speed;
		_startSpeed = _speed;
#if UNITY_ANDROID
		_touchScreen = true;
		Debug.Log("Android");
#else
		_touchScreen = false;
		Debug.Log("Not Android");
#endif
	}

	private void Start()
	{
		if (_touchScreen)
			SwipeManager.Instance.OnSwipeDetected += OnSwipeDetected;

	}

	private void Update()
	{
		if (!_touchScreen)
			CheckMoveInput();
	}

	private void FixedUpdate()
	{
		MoveForward();
		if (_jump)
		{
			Jump();
			_jump = false;
			return;
		}
	}

	private void OnSwipeDetected(SwipeDirection dir)
	{
		switch (dir)
		{
			case SwipeDirection.None:
				break;
			case SwipeDirection.Up:
				_jump = true;
				break;
			case SwipeDirection.Down:
				Slice();
				break;
			case SwipeDirection.Left:
				ChangeLane(false);
				break;
			case SwipeDirection.Right:
				ChangeLane(true);
				break;
		}
	}

	private void CheckMoveInput()
	{
		if (Input.GetButton("HorizontalRight"))
			ChangeLane(true);
		if(Input.GetButton("HorizontalLeft"))
			ChangeLane(false);
		if (Input.GetButtonDown("Slice"))
			Slice();

		if (Input.GetButtonDown("Jump"))
			_jump = true;
	}

	private void MoveForward()
	{
		Vector3 velocity = _rigidbody.velocity;
		velocity.z = _speed;
		_rigidbody.velocity = velocity;
	}

	private void ChangeLane(bool right)
	{
		if (!_isGrounded || _laneChangeInProgress || right && (_currentLane == 1 || !_rightSensor.IsFree) || !right && (_currentLane == -1 || !_leftSensor.IsFree))
			return;
		StartCoroutine(ChangeLaneRoutine(right));
		_laneChangeInProgress = true;
	}

	private void Jump()
	{
		if (_isSliding || !_isGrounded)
			return;
		_rigidbody.AddForce(transform.up * _jumpForce, ForceMode.Impulse);
		StartCoroutine(JumpRoutin());
	}

	private void Slice()
	{
		if (_isSliding || !_isGrounded)
			return;
		StartCoroutine(SliceRountine());
	}

	public void UseScorePowerUp(int score)
	{
		GameMaster.Instance.AddScore(score);
		if (OnPowerUpPickedUp != null)
			OnPowerUpPickedUp.Invoke(PowerUpType.Score);
	}

	public void UseNoGravityPowerUp(float duration)
	{
		_noGravityPowerUpOn = true;
		if (_noGravityRoutine != null)
			StopCoroutine(_noGravityRoutine);
		_noGravityRoutine = StartCoroutine(TurnOffPowerUp(duration, _noGravitySign, PowerUpType.NoGravity));
		if (OnPowerUpPickedUp != null)
			OnPowerUpPickedUp.Invoke(PowerUpType.NoGravity);
	}

	public void UseIndestructiblePowerUp(float duration)
	{
		_indestructible = true;
		if (_indestructibleRoutine != null)
			StopCoroutine(_indestructibleRoutine);
		_indestructibleRoutine = StartCoroutine(TurnOffPowerUp(duration, _indestructibleSign, PowerUpType.Indestructible));
		if (OnPowerUpPickedUp != null)
			OnPowerUpPickedUp.Invoke(PowerUpType.Indestructible);
	}

	IEnumerator TurnOffPowerUp(float duration, GameObject sign, PowerUpType powerUpType)
	{
		float timer = duration * _powerBlinkingPercent;
		sign.SetActive(true);
		yield return new WaitForSeconds(timer);
		while (timer < duration)
		{
			sign.SetActive(!sign.activeSelf);
			float waitTime = Mathf.Max((duration - timer) * 0.1f, 0.001f);
			yield return new WaitForSeconds(waitTime);
			timer += waitTime;
		}
		sign.SetActive(false);
		switch(powerUpType)
		{
			case PowerUpType.NoGravity:
				_noGravityPowerUpOn = false;
				break;
			case PowerUpType.Indestructible:
				_indestructible = false;
				break;
		}
	}

	IEnumerator JumpRoutin()
	{
		yield return new WaitForSeconds(_groundSensorDelayAfterJump);
		_groundSensor.gameObject.SetActive(true);
	}

	IEnumerator SliceRountine()
	{
		_isSliding = true;

		Vector3 originalScale = _modelHolder.transform.localScale;
		_modelHolder.transform.localScale = Vector3.one * _sliceScale;

		//_rigidbody.AddForce(Vector3.down * 100, ForceMode.Impulse);

		yield return new WaitForSeconds(_sliceDuration);

		_modelHolder.transform.localScale = originalScale;

		_isSliding = false;
	}

	IEnumerator ChangeLaneRoutine(bool right)
	{
		float timer = 0.0f;
		_currentLane += right ? 1 : -1;
		float weightedSignDistance = (right ? 1 : -1) * _laneDistance / _laneChangeDuration;

		while (timer < _laneChangeDuration)
		{
			Vector3 newPosition = gameObject.transform.position;
			newPosition.x += weightedSignDistance * Time.deltaTime;
			gameObject.transform.position = newPosition;
			timer += Time.deltaTime;
			yield return null;
		}
		float endXCoor = 0;
		switch (_currentLane)
		{
			case -1:
				endXCoor = _middleLaneX - _laneDistance;
				break;
			case 0:
				endXCoor = _middleLaneX;
				break;
			case 1:
				endXCoor = _middleLaneX + _laneDistance;
				break;

		}
		Vector3 endPos = gameObject.transform.position;
		endPos.x = endXCoor;
		gameObject.transform.position = endPos;
		_laneChangeInProgress = false;
	}
}