﻿using UnityEngine;

public class NoGravityPowerUp : PowerUp 
{
	[SerializeField] private float _duration = 30.0f;

	private void Awake()
	{
		_powerUpType = PowerUpType.NoGravity;
	}

	protected override void UsePowerUp()
	{
		Debug.Log("<color=yellow>power up</color>");
		Player.Instance.UseNoGravityPowerUp(_duration);
	}



}

