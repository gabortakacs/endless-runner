﻿using UnityEngine;

public class IndestructiblePowerUp : PowerUp
{

	[SerializeField] private float _duration = 15.0f;

	private void Awake()
	{
		_powerUpType = PowerUpType.Indestructible;
	}

	protected override void UsePowerUp()
	{
		Player.Instance.UseIndestructiblePowerUp(_duration);
	}
}

