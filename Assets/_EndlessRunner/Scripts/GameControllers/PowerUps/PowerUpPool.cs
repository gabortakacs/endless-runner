﻿using UnityEngine;

public class PowerUpPool : ObjectPool 
{

	[SerializeField] private int _chanceToSpawn = 50;

	public int ChanceToSpawn { get { return _chanceToSpawn; } }

}

