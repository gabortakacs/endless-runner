﻿using UnityEngine;

public class ScorePowerUp : PowerUp
{
	[SerializeField] private int _point = 50;

	private void Awake()
	{
		_powerUpType = PowerUpType.Score;
	}

	protected override void UsePowerUp()
	{
		Player.Instance.UseScorePowerUp(_point);
	}
}

