﻿using UnityEngine;

public enum PowerUpType
{
	None,
	NoGravity,
	Score,
	Indestructible
}

public abstract class PowerUp : ObjectPoolEntity 
{
	protected abstract void UsePowerUp();
	protected PowerUpType _powerUpType;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != LayerMasks.PlayerSingleLayerMask)
			return;

		SoundMaster.Instance.PlayPowerUpSound(_powerUpType);
		UsePowerUp();

		if (_owner)
			_owner.Push(this);
	}




}

