﻿using UnityEngine;

public class TargetFollow : MonoBehaviour 
{
	[SerializeField] private Transform _target;
	[SerializeField] private float _posY;

	private void Update()
	{
		Vector3 targetPos = _target.position;
		targetPos.y = _posY;
		transform.position = targetPos;
	}

}

