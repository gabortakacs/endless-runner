﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScoreRefresher : MonoBehaviour 
{

	[SerializeField] private Text _text;

	private void OnEnable()
	{
		_text.text = ((int)GameMaster.Instance.Score).ToString();
	}
}

