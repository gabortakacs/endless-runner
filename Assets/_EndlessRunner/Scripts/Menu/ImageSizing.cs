﻿using UnityEngine;
using UnityEngine.UI;

public class ImageSizing : MonoBehaviour 
{
 	[SerializeField] private RectTransform _rectTransform;
	[SerializeField] private bool _vertical = true;

	private void Awake()
	{
		GridLayoutGroup gridLayoutGroup = GetComponentInParent<GridLayoutGroup>();
		if(_vertical)
		{
			_rectTransform.sizeDelta = Vector2.one * gridLayoutGroup.cellSize.x;
			//_rectTransform.rect.height = _targetRectTransform.rect.height;
		}
		else
		{
			_rectTransform.sizeDelta = Vector2.one * gridLayoutGroup.cellSize.y;
		}
	}




}

