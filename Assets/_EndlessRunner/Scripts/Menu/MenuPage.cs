﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuPage : MonoBehaviour
{

	[SerializeField] private GameObject _defaultSelectedObject;
	[SerializeField] private Canvas _canvas;
	[SerializeField] private GameObject _mainText;
	[SerializeField] private GameObject[] _subObjects;

	public GameObject DefaultSelectedObject { get { return _defaultSelectedObject; } }

	private void Start()
	{
		SetMainTextPosition();
		SetSubObjectsPosition();
	}

	private void SetMainTextPosition()
	{
		if (_mainText == null)
			return;
		SetAnchorPos(_mainText.GetComponent<RectTransform>(), MenuPageSetup.Instance.MainTextPosYGlobal);
	}

	private void SetSubObjectsPosition()
	{
		for(int i = 0; i < _subObjects.Length; ++i)
		{
			SetAnchorPos(_subObjects[i].GetComponent<RectTransform>(), MenuPageSetup.Instance.SubObjectStartPosYGlobal + MenuPageSetup.Instance.SubObjectOffsetY * i);
		}
	}

	private void SetAnchorPos(RectTransform rectTransform, int positionY)
	{
		Vector2 anchorPos = rectTransform.anchoredPosition;
		anchorPos.y = positionY;
		rectTransform.anchoredPosition = anchorPos;
	}
#if UNITY_EDITOR || UNITY_STANDALONE
	public void OnEnable()
	{
		StartCoroutine(SetDefaultSelectedObjectRoutine());
	}

	private IEnumerator SetDefaultSelectedObjectRoutine()
	{
		EventSystem.current.SetSelectedGameObject(null);
		yield return new WaitForEndOfFrame();
		EventSystem.current.SetSelectedGameObject(_defaultSelectedObject);
	}
#endif
}

