﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct AchievementElementLongText
{
	public AchievementType condition;
	public string longDescription;
}

public class AchievementLongTextHelper : MonoBehaviour 
{
	[SerializeField] private Text _text;
	[SerializeField] private GameObject _achivementElementsParent;
	[SerializeField] private string _token = "???";
	[SerializeField] private AchievementElementLongText[] _longTexts;

	private Dictionary<AchievementType, string> _longTextDictonary = new Dictionary<AchievementType, string>();
	private void Awake()
	{
		longTextsToDictionary();
		AchievementElement[] achievementElements = _achivementElementsParent.GetComponentsInChildren<AchievementElement>();
		for(int i = 0; i < achievementElements.Length; ++i)
		{
			AchievementElement currentElement = achievementElements[i];
			currentElement.LongText = _text;
			currentElement.LongDescription = tokenToText(_longTextDictonary[currentElement.ConditionType], currentElement.ConditionScore.ToString());
		}
	}

	private string tokenToText(string text, string replacement)
	{
		text = text.Replace(_token, replacement);
		return text;
	}

	private void longTextsToDictionary()
	{
		for (int i = 0; i < _longTexts.Length; ++i)
		{
			_longTextDictonary.Add(_longTexts[i].condition, _longTexts[i].longDescription);
		}
	}

}

