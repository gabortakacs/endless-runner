﻿using UnityEngine;

public class MenuPageSetup : Singleton<MenuPageSetup> 
{

	[SerializeField] private int _mainTextPosYGlobal = -50;
	[SerializeField] private int _subObjectStartPosYGlobal = -150;
	[SerializeField] private int _subObjectOffsetY = -50;

	public int MainTextPosYGlobal { get { return _mainTextPosYGlobal; } }
	public int SubObjectStartPosYGlobal { get { return _subObjectStartPosYGlobal; } }
	public int SubObjectOffsetY { get { return _subObjectOffsetY; } }

}

