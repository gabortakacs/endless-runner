﻿using System;
using UnityEngine;
using UnityEngine.UI;

public enum AchievementType
{
	Score,
	Time,
	PowerUpAll,
	PowerUpNoGravity,
	PowerUpScore,
	PowerUpIndestructiblePowerUp
}

public class AchievementElement : MonoBehaviour 
{
	[SerializeField] private AchievementType _conditionType;
	[SerializeField] private int _conditionScore;

	[Header("Texts")]
	[SerializeField] private string _sortDescription;
	[SerializeField] private Text _sortText;
	[SerializeField] private Button _achievementButton;

	[Header("Image")]
	[SerializeField] private RectTransform _imageHolder;
	[SerializeField] private Image _image;
	[SerializeField] private bool _vertical = true;
	[SerializeField] private Sprite _activeImage;
	[SerializeField] private Sprite _deactiveImage;

	private Text _longText;
	public bool _performed = false;
	private string _longDescription;

	public string LongDescription { set { _longDescription = value; } }
	public int ConditionScore { get { return _conditionScore; } }
	public AchievementType ConditionType { get { return _conditionType; } }
	public Text LongText { set { _longText = value; } }
	public bool Performed
	{ set
		{
			_performed = value;
			ChooseImage();
		}
	}

	private void Awake()
	{
		ResizeImage();
		_sortText.text = _sortDescription;
	}

	private void OnEnable()
	{
		ChooseImage();
	}

	private void ChooseImage()
	{
		_image.sprite = _performed ? _activeImage : _deactiveImage;
	}

	private void ResizeImage()
	{
		GridLayoutGroup gridLayoutGroup = GetComponentInParent<GridLayoutGroup>();
		if (_vertical)
		{
			_imageHolder.sizeDelta = Vector2.one * gridLayoutGroup.cellSize.x;
			//_rectTransform.rect.height = _targetRectTransform.rect.height;
		}
		else
		{
			_imageHolder.sizeDelta = Vector2.one * gridLayoutGroup.cellSize.y;
		}
		_achievementButton.onClick.AddListener(PrintLongText);
	}

	private void PrintLongText()
	{
		_longText.text = _longDescription;
	}
}

